
To Do

## Shakti Arduino board specific files

This project provides support for the Shakti Arduino1 development board.

## Developer info

This section is intended to developers who plan to include this library in their own projects.

### Prerequisites

A recent [`xpm`](https://www.npmjs.com/package/xpm), which is a portable [Node.js](https://nodejs.org/) command line application.

Compiling the source code requires a modern C++ compiler, preferably GCC 5 or higher. 

### Easy install

This package is available as [`@iitm_shakti/arduino-board`](https://www.npmjs.com/package/@iitm_shakti/arduino-board) from the `npmjs.com` registry; with `xpm` available, installing the latest version of the package is quite easy:

```console
$ xpm install @iitm_shakti/arduino-board
```

This package is also available from [GitLab](https://gitlab.com/shaktiproject/software/shakti-arduino-board-xpack.git):

```console
$ git clone https://gitlab.com/shaktiproject/software/shakti-arduino-board-xpack.git
```

### How to use

The standard way to include the board files is

```c
#include <micro-os-plus/board.h>
```

### Macros

* `SHAKTI_ARDUINO_BOARD`

## Maintainer info

### How to publish

* commit all changes
* update `CHANGELOG.md`; commit with a message like _CHANGELOG: prepare v0.1.2_
* `npm version patch`
* push all changes to GitHub
* `npm publish`

## License

TODO
